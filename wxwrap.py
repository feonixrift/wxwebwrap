#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
TRY to wxpython wrap bokeh serve in a way that doesn't footgun itself.
"""

import wx
import wx.html2
from subprocess import Popen
import time

class MainFrame(wx.Frame):
    def __init__(self, parent, title):
        wx.Frame.__init__(self, parent, title=title, size=(1024,768))

        self.initMenu()
        self.statusbar = self.CreateStatusBar()
        sizer = wx.BoxSizer(wx.VERTICAL)

        self.bw = BokehWrap(self)
        sizer.Add(self.bw, 1, flag=wx.EXPAND)

        self.SetSizer(sizer)
        self.Show(True)

    def initMenu(self):
        fileMenu = wx.Menu()

        menuBar = wx.MenuBar()
        menuBar.Append(fileMenu, "&File")

        self.SetMenuBar(menuBar)

class BokehWrap(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent)

        self.canvas = wx.html2.WebView.New(self, size=(800,600), url="http://localhost:5006/bkh")
        self.canvas.Refresh()

    def OnPaint(self):
        self.canvas.Refresh()

# BUG not the proper way to handle timing
command = ['bokeh', 'serve', 'bkh.py']
bokeh_server = Popen(command)
time.sleep(1)
# use https://effbot.org/zone/python-with-statement.htm instead
try:
    app = wx.App(False)
    frame = MainFrame(None, "wrtgf")
    app.MainLoop()
finally:
    bokeh_server.terminate()
